import 'dart:io';
import 'dart:math';

mixin Deck {
  Random random = new Random();

  List<int> card = [];
  List<int> cardInHand_me = [];
  List<int> cardInHand_bot1 = [];
  List<int> cardInHand_bot2 = [];

  List<int> randomCard() {
    List? cardNumber = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 , 12 , 13 ,14 ,15];
    for (int i = 0; i < cardNumber.length ; i++) {
      int randomCard = cardNumber[random.nextInt(cardNumber.length)];
      card.add(randomCard);
      cardNumber.remove(randomCard);
    }
    return card;
  }

  void giveCrad() {
    cardInHand_me = addCardInHand(cardInHand_me);
    cardInHand_bot1 = addCardInHand(cardInHand_bot1);
    cardInHand_bot2 = addCardInHand(cardInHand_bot2);
  }

  List<int> addCardInHand(List<int> cardInHand) {
    cardInHand.add(card[0]);
    card.remove(card[0]);
    return cardInHand;
  }
  
}
