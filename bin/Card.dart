import 'dart:ffi';
import 'dart:io';
import 'Deck.dart';
import 'Hand.dart';


abstract class Card with Deck{
  String rank;
  String suit;
  
  Card(this.rank, this.suit);
  
  toString() {
    return '$rank of $suit';
  }
}
