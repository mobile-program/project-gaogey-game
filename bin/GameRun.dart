import 'dart:io';
import 'Deck.dart';
import 'Game.dart';
import 'Player.dart';

class GameRun with Deck,Game,Player {

  void startGame(){
    // money_me = 10;
    // money_bot1 = 10;
    // money_bot2 = 10;
    welcomeGame();
    boardGame();
  }
  void boardGame(){
    money_me = 10;
    money_bot1 = 10;
    money_bot2 = 10;
    while (true) {
        if (money_me == 0 || money_bot1 == 0 || money_bot2 == 0) break;
        newGame();
        // || status_bot1 == true || status_bot2 == true
        while (true) {
          if (status_me == true) { //set ค่าจาก total ในคลาส GameRun ได้ แต่ไม่สามารถนำค่าไปดูผลแพ้ชนะได้
              total_me = cardInHand_me[0];
              total_bot1 = cardInHand_bot1[0];
              total_bot2 = cardInHand_bot2[0];
              printResult();
              break;
          } else {
            if (card.isNotEmpty) {
              takeTurn_me();
              takeTurn_bot1();
              takeTurn_bot2();
            }
            else 
            if (card.isEmpty) {
              status_me = true;
              status_bot1 = true;
              status_bot2 = true;
            }
          }
        }
      }
  }
  void printResult(){
    print('_________________________________________________________________');
    printWinnerIs();
    print('MY CARDS      : $cardInHand_me');
    print('BOT1 CARDS     : $cardInHand_bot1');
    print('BOT2 CARDS     : $cardInHand_bot2');
    print('COMMON FUNDS : $allMoney');
    print('MY MONEY IS   : $money_me');
    print('BOT1 MONEY IS  : $money_bot1');
    print('BOT2 MONEY IS  : $money_bot2');
  }
  void showMomey(){
  print('COMMON FUNDS : $allMoney');
  print('MY MONEY IS   : $money_me');
  print('BOT1 MONEY IS  : $money_bot1');
  print('BOT2 MONEY IS  : $money_bot2');
  // print('MY MONEY IS   : ${allMoney+money_me}');
  }

  void takeTurn_me(){
    card_me();
    print('My Card: $cardInHand_me');
    print('BOT1  : ? ');
    print('BOT2  : ? ');
    print(' y = BET \n n = FOLD \n o = OPEN');
    showMomey();
    stdout.write('You Choice Is : ');
    String? playerChoice = stdin.readLineSync();
    print('');
    while (true) {
      if (playerChoice == 'y' || playerChoice == 'n' || playerChoice == 'o') {
        switch (playerChoice) {
          case 'y':
            print('ME : " BET. " ');
            money_me = money_me-1;
            allMoney = allMoney+1;
            status_me = false; //false
            print('');
            break;
          case 'n':
            print('ME : " FOLD. " ');
            money_me = money_me;
            status_me = true; //true
            print('');
            break;
          case 'o':
            print('ME : " OPEN. " ');
            money_me = money_me;
            status_me = true; //true
            print('');
            break;
        }
        break;
      } 
      // else {
      //   while (true) {
      //     print('PLEASE ENTER y = BET \n n = FOLD \n o = OPEN');
      //     playerChoice = stdin.readLineSync();
      //     if (playerChoice == 'y' || playerChoice == 'n' ||playerChoice == 'o') break;
      //   }
      // }
    }

  }
  void takeTurn_bot1() {
    var total = cardInHand_bot1[0];
    if (total == 15){
      print('bot1 : "OPEN"');
      status_bot1 = true;
      print('');
    } else if (total > 10) {
      print('Bot1 : " BET. " ');
      money_bot1 = money_bot1-1;
      allMoney = allMoney+1;
      status_bot1 = false;
      print('');
    } else if (total <5) {
      print('Bot1 : " FOLD. " ');
      status_bot1 = true;
      print('');
    } else {
      print('Bot1 : " BET. " ');
      money_bot1 = money_bot1-1;
      allMoney = allMoney+1;
      status_bot1 = false;
      print('');
    }
  }
  void takeTurn_bot2() {
    var total = cardInHand_bot2[0];
    if (total == 15){
      print('bot1 : "  OPEN.  "');
      status_bot2 = true;
      print('');
    } else if (total > 12) {
      print('Bot2 : " BET. " ');
      money_bot2 = money_bot2-1;
      allMoney = allMoney+1;
      status_bot2 = false;
      print('');
    } else if (total < 4) {
      print('Bot2 : " FOLD. " ');
      status_bot2 = true;
      print('');
    } else {
      print('Bot2 : " FOLD. " ');
      status_bot2 = true;
      print('');
    }
  }
  void newGame(){
    allMoney = 0;
    money_me = 0;
    money_bot1 = 0;
    money_bot2 = 0;

    card = [];

    cardInHand_me = [];
    cardInHand_bot1 = [];
    cardInHand_bot2 = [];

    card = randomCard();
    giveCrad();
  }
  
}

