import 'dart:io';
import 'dart:math';

mixin Game {
  int? total_me = 0;
  int? total_bot1 = 0;
  int? total_bot2 = 0;

  int money_me = 10;
  int money_bot1 = 10;
  int money_bot2 = 10;

  int allMoney = 0;

  bool status_me = false;
  bool status_bot1 = false;
  bool status_bot2 = false;

  // bool status = false;
  String? checkWin(int total_me ,int total_bot1 ,int total_bot2){ //ส่งค่าการ์ดจาก GameRun มา total_me ??
    if(total_me > total_bot1){
      if(total_me > total_bot2){
        // money_me = money_me + money_bot1 + money_bot2;
        return 'YOU WIN'; 
      }else{
        // money_bot2 = money_me + money_bot1 + money_bot2;
        return 'BOT2 WIN';
      }
    }else if(total_bot1 > total_bot2){
      // money_bot1 = money_me + money_bot1 + money_bot2;
      return 'BOT1 WIN';
    }else {
      // money_bot2 = money_me + money_bot1 + money_bot2;
      return 'BOT2 WIN';
    }
  }
  void printWinnerIs() {
    String? winner = checkWin(total_me!,total_bot1!,total_bot2!);
    if(winner == 'YOU WIN'){
      print(' __   __  _______  __   __    _     _  ___   __    _  __   __  ');
      print('|  | |  ||       ||  | |  |  | | _ | ||   | |  |  | ||  | |  | ');
      print('|  |_|  ||   _   ||  | |  |  | || || ||   | |   |_| ||  | |  | ');
      print('|       ||  | |  ||  |_|  |  |       ||   | |       ||  | |  | ');
      print('|_     _||  |_|  ||       |  |       ||   | |  _    ||__| |__| ');
      print('  |   |  |       ||       |  |   _   ||   | | | |   | __   __  ');
      print('  |___|  |_______||_______|  |__| |__||___| |_|  |__||__| |__| ');
      print('');
      money_me = money_me + allMoney;
    }else if (winner == 'BOT1 WIN') {
      print(
          ' __   __  _______  __   __    ___      _______  _______  _______  __  ');
      print(
          '|  | |  ||       ||  | |  |  |   |    |       ||       ||       ||  | ');
      print(
          '|  |_|  ||   _   ||  | |  |  |   |    |   _   ||  _____||    ___||  | ');
      print(
          '|       ||  | |  ||  |_|  |  |   |    |  | |  || |_____ |   |___ |  | ');
      print(
          '|_     _||  |_|  ||       |  |   |___ |  |_|  ||_____  ||    ___||__| ');
      print(
          '  |   |  |       ||       |  |       ||       | _____| ||   |___  __  ');
      print(
          '  |___|  |_______||_______|  |_______||_______||_______||_______||__| ');
      print('');
      money_bot1 = money_bot1 + allMoney;
    }else if (winner == 'BOT2 WIN') {
      print(
          ' __   __  _______  __   __    ___      _______  _______  _______  __  ');
      print(
          '|  | |  ||       ||  | |  |  |   |    |       ||       ||       ||  | ');
      print(
          '|  |_|  ||   _   ||  | |  |  |   |    |   _   ||  _____||    ___||  | ');
      print(
          '|       ||  | |  ||  |_|  |  |   |    |  | |  || |_____ |   |___ |  | ');
      print(
          '|_     _||  |_|  ||       |  |   |___ |  |_|  ||_____  ||    ___||__| ');
      print(
          '  |   |  |       ||       |  |       ||       | _____| ||   |___  __  ');
      print(
          '  |___|  |_______||_______|  |_______||_______||_______||_______||__| ');
      print('');
      money_bot2 = allMoney + money_bot2;
    } 
    // else {
    //   print(' _______  ___   _______    __   __  ');
    //   print('|       ||   | |       |  |  | |  | ');
    //   print('|_     _||   | |    ___|  |  | |  | ');
    //   print('  |   |  |   | |   |___   |  | |  | ');
    //   print('  |   |  |   | |    ___|  |__| |__| ');
    //   print('  |   |  |   | |   |___    __   __  ');
    //   print('  |___|  |___| |_______|  |__| |__| ');
    //   print('');
    // }
  }
  void card_me(){
    int? cardPlayer = total_me;
    switch(total_me){
      case 1:
      print('.------.');
      print('|1.---. |');
      print('| :   : |');
      print('|  _ _  |');
      print('|  --- 1|');
      print(' ------ ');
      break;
      case 2:
      print('.------.');
      print('|2.---. |');
      print('| :   : |');
      print('|  _ _  |');
      print('|  --- 2|');
      print(' ------ ');
      break;
      case 3:
      print('.------.');
      print('|3.---. |');
      print('| :   : |');
      print('|  _ _  |');
      print('|  --- 3|');
      print(' ------ ');
      break;
      case 4:
      print('.------.');
      print('|4.---. |');
      print('| :   : |');
      print('|  _ _  |');
      print('|  --- 4|');
      print(' ------ ');
      break;
      case 5:
      print('.------.');
      print('|5.---. |');
      print('| :   : |');
      print('|  _ _  |');
      print('|  --- 5|');
      print(' ------ ');
      break;
    }

  }
  void welcomeGame(){
    print(' .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.');
    print('| .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |');
    print('| | _____  _____ | || |  _________   | || |   _____      | || |     ______   | || |     ____     | || | ____    ____ | || |  _________   | |');
    print('| ||_   _||_   _|| || | |_   ___  |  | || |  |_   _|     | || |   .  ___  |  | || |   .     `.   | || ||_      /   _|| || | |_   ___  |  | |');
    print('| |  | | /  | |  | || |   | |_   _|  | || |    | |       | || |  / .      |  | || |  /  .--.     | || |  |    /   |  | || |   | |_    |  | |');
    print('| |  | |/   | |  | || |   |  _|  _   | || |    | |   _   | || |  | |         | || |  | |    | |  | || |  | |   /| |  | || |   |  _|  _   | |');
    print('| |  |   /    |  | || |  _| |___/ |  | || |   _| |__/ |  | || |    `.___.    | || |     --    /  | || | _| |_ /_| |_ | || |  _| |___/ |  | |');
    print('| |  |__/   __|  | || | |_________|  | || |  |________|  | || |   `._____.   | || |   `.____.    | || ||_____||_____|| || | |_________|  | |');
    print('| |              | || |              | || |              | || |              | || |              | || |              | || |              | |');
    print('|  --------------  ||  --------------  ||  --------------  ||  --------------  ||  --------------  ||  --------------  ||  --------------  |');
    print(' ----------------    ----------------    ----------------    ----------------    ----------------    ----------------    ---------------- ');
  }

}